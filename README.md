<!--
 apito-aspnet is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 apito-aspnet is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with apito-aspnet. If not, see <https://www.gnu.org/licenses/>.
-->

[Symfony](https://gitlab.com/easbarba/apito-symfony) [Express](https://gitlab.com/easbarba/apito-express) |  [Vue.js](https://gitlab.com/easbarba/apito-vue) | [Quarkus](https://gitlab.com/easbarba/apito-quarkus) |  [Main](https://gitlab.com/easbarba/apito)

# Apito | ASP.NET

Evaluate soccer referees' performance.

## Stack

- [ASP.NET](https://spring.io/projects/spring-boot) { Framework, WebFlux, Security, Data }
- [Keycloak](https://www.keycloak.org) via container
- [MapStruct](https://mapstruct.org/): DTO Bean mappings
- [TestContainers](https://testcontainers.com)
- [PostgreSQL](https://www.postgresql.orgf/) 
- [Koto](https://gitlab.com/easbarba/koto): API Testing tool, just like Postman/Insomnia/Bruno, for CLI power users.
- [Makefile](Makefile) : speed up development some `make` rules are provided. 
- [Podman](https://podman.io) [pods](https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods#shortcut_to_create_pods) that offers a rootless k8s's pods like experience to local development!
- [GNU Guix](https://guix.gnu.org): install all system dependencies in easy undo isolated experience.

### Endpoints

| Endpoints            | Description                   |
|----------------------|-------------------------------|
| /api//VERSION        | Apito general information     |
| /api/VERSION/openapi | openAPI documentation as json |
| /api/VERSION/swagger | openAPI UI swagger            |


### Resources

| Endpoints             | Methods                               |
|-----------------------|---------------------------------------|
| /api/VERSION/referees | OPTION, GET, POST, PUT, PATCH, DELETE |
    
PS: Non-Safe methods require a bearer token. 
PS2: All information about API design, openAPI, and related documentation are found at the `docs`.

## [Makefile](Makefile)

The Makefile file provides all sort of handy tasks. It relies on the `.env.*`
files, so make sure to match due variables.

| goals | description                          |
|-------|--------------------------------------|
| up    | spin up containers for development   |
| down  | spin down containers for development |

## [Podman Pods](https://podman.io)

Podman's pod offers a rootless k8s's pods-like experience to local development, [check it out!](https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods#shortcut_to_create_pods)

Some targets are using pods to boot up necessary containers, look at the `Makefile` for more infromation.

![podman pod](podman_pod.png)

For more information on development check out the `CONTRIBUTING.md` document.

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
